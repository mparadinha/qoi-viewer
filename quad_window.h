#ifndef QUAD_WINDOW_H // --------- start of header ---------
#define QUAD_WINDOW_H

#define GLFW_INCLUDE_NONE // don't include any GL headers, because we have our own
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    GLFWwindow* window;
    uint32_t quad_handle;
    uint32_t shader_handle;
    uint32_t texture_handle;
    float ratio;
} QuadWindow;

QuadWindow quad_window_create(uint32_t win_w, uint32_t win_h, char* title);

void quad_window_draw(QuadWindow quad_window);

// update the texture data drawn on the quad
void quad_window_update(QuadWindow* quad_window, uint32_t img_w, uint32_t img_h, uint8_t* data);

#endif // --------- end of header ---------

#ifdef QUAD_WINDOW_IMPLEMENTATION

#define massert(cond, msg) if (!(cond)) { fprintf(stderr, msg); abort(); }

#include <dlfcn.h>

#define GL_FALSE 0
#define GL_TRUE 1
#define GL_BLEND 0x0be2
#define GL_ONE_MINUS_SRC_ALPHA 0x0303
#define GL_SRC_ALPHA 0x0302
#define GL_UNSIGNED_SHORT 0x1403
#define GL_UNSIGNED_INT 0x1405
#define GL_VERSION 0x1f02
#define GL_TEXTURE0 0x84c0
#define GL_TEXTURE_2D 0x0de1
#define GL_LINEAR 0x2601
#define GL_TEXTURE_MAG_FILTER 0x2800
#define GL_TEXTURE_MIN_FILTER 0x2801
#define GL_TEXTURE_WRAP_S 0x2802
#define GL_TEXTURE_WRAP_T 0x2803
#define GL_CLAMP_TO_BORDER 0x812d
#define GL_RGBA	0x1908
#define GL_FLOAT 0x1406
#define GL_FRAGMENT_SHADER 0x8b30
#define GL_VERTEX_SHADER 0x8b31
#define GL_ARRAY_BUFFER 0x8892
#define GL_STATIC_DRAW 0x88e4
#define GL_TRIANGLES 0x0004
#define GL_COLOR_BUFFER_BIT 0x00004000
#define GL_ELEMENT_ARRAY_BUFFER 0x8893
#define GL_UNSIGNED_BYTE 0x1401
#define GL_UNPACK_ALIGNMENT 0x0cf5

#define DECL_FUNC_PTR(OUT, IN, STR) OUT (*STR) IN;
DECL_FUNC_PTR(void, (int32_t x, int32_t y, int32_t w, int32_t h), glViewport)
DECL_FUNC_PTR(void, (uint32_t id), glActiveTexture)
DECL_FUNC_PTR(void, (uint32_t n, uint32_t* textures), glGenTextures)
DECL_FUNC_PTR(void, (uint32_t target, uint32_t texture), glBindTexture)
DECL_FUNC_PTR(void, (uint32_t target, uint32_t pname, uint32_t param), glTexParameteri)
DECL_FUNC_PTR(void, (uint32_t target, int level, int internalformat, int w, int h, int border,
    uint32_t format, uint32_t type, const void* data), glTexImage2D)
DECL_FUNC_PTR(void, (uint32_t target), glGenerateMipmap)
DECL_FUNC_PTR(uint32_t, (uint32_t type), glCreateShader)
DECL_FUNC_PTR(void, (uint32_t id), glCompileShader)
DECL_FUNC_PTR(uint32_t, (void), glCreateProgram)
DECL_FUNC_PTR(void, (uint32_t, int, const char**, const int*), glShaderSource)
DECL_FUNC_PTR(void, (uint32_t prog_id, uint32_t shader_id), glAttachShader)
DECL_FUNC_PTR(void, (uint32_t id), glLinkProgram)
DECL_FUNC_PTR(void, (int, uint32_t*), glGenVertexArrays)
DECL_FUNC_PTR(void, (int, uint32_t*), glGenBuffers)
DECL_FUNC_PTR(void, (uint32_t), glBindVertexArray)
DECL_FUNC_PTR(void, (uint32_t type, uint32_t id), glBindBuffer)
DECL_FUNC_PTR(void, (uint32_t target, long size, const void* data, uint32_t usage), glBufferData)
DECL_FUNC_PTR(void, (uint32_t idx, int size, uint32_t type, bool norm, uint32_t stride,
    const void* ptr), glVertexAttribPointer)
DECL_FUNC_PTR(void, (uint32_t idx), glEnableVertexAttribArray)
DECL_FUNC_PTR(void, (uint32_t mask), glClear)
DECL_FUNC_PTR(void, (uint32_t id), glUseProgram)
DECL_FUNC_PTR(void, (uint32_t type, int first, uint32_t count), glDrawArrays)
DECL_FUNC_PTR(void, (int32_t location, float value), glUniform1f)
DECL_FUNC_PTR(void, (int32_t location, uint32_t value), glUniform1i)
DECL_FUNC_PTR(int, (uint32_t prog_id, const char*), glGetUniformLocation)
DECL_FUNC_PTR(void, (uint32_t), glEnable)
DECL_FUNC_PTR(void, (uint32_t sfactor, uint32_t dfactor), glBlendFunc)
DECL_FUNC_PTR(void, (uint32_t mode, int32_t count, uint32_t type, const void* indices), glDrawElements)
DECL_FUNC_PTR(void, (uint32_t pname, int32_t param), glPixelStorei) 
#undef DECL_FUNC_PTR

void load_gl() {
    void* lib = dlopen("libGL.so", RTLD_NOW | RTLD_GLOBAL);
    massert(lib, "failed to open libGL.so\n");

    // this macro assumes that the function was already declared
    #define LOAD_FUNC(FN) \
        FN = dlsym(lib, #FN); massert(FN, "failed to load function " #FN);
    LOAD_FUNC(glViewport)
    LOAD_FUNC(glActiveTexture)
    LOAD_FUNC(glGenTextures)
    LOAD_FUNC(glBindTexture)
    LOAD_FUNC(glTexParameteri)
    LOAD_FUNC(glTexImage2D)
    LOAD_FUNC(glGenerateMipmap)
    LOAD_FUNC(glCreateShader)
    LOAD_FUNC(glCompileShader)
    LOAD_FUNC(glCreateProgram)
    LOAD_FUNC(glShaderSource)
    LOAD_FUNC(glAttachShader)
    LOAD_FUNC(glLinkProgram)
    LOAD_FUNC(glGenVertexArrays)
    LOAD_FUNC(glGenBuffers)
    LOAD_FUNC(glBindVertexArray)
    LOAD_FUNC(glBindBuffer)
    LOAD_FUNC(glBufferData)
    LOAD_FUNC(glVertexAttribPointer)
    LOAD_FUNC(glEnableVertexAttribArray)
    LOAD_FUNC(glClear)
    LOAD_FUNC(glUseProgram)
    LOAD_FUNC(glDrawArrays)
    LOAD_FUNC(glUniform1f)
    LOAD_FUNC(glUniform1i)
    LOAD_FUNC(glGetUniformLocation)
    LOAD_FUNC(glEnable)
    LOAD_FUNC(glBlendFunc)
    LOAD_FUNC(glDrawElements)
    LOAD_FUNC(glPixelStorei)
    #undef LOAD_FUNC

    dlclose(lib);
}

QuadWindow quad_window_create(uint32_t win_w, uint32_t win_h, char* title) {
    massert(glfwInit(), "failed to initialize glfw\n")
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    GLFWwindow* window = glfwCreateWindow(win_w, win_h, title, NULL, NULL);
    massert(window, "failed to create glfw window\n");
    glfwMakeContextCurrent(window);

    load_gl();

    // enable alpha blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // create shader program
    const char* vert_shader_src =
        "#version 330 core\n"
        "layout (location = 0) in vec2 vertex_pos;\n" 
        "uniform float x_ratio;\n"
        "uniform float y_ratio;\n"
        "out vec2 uv; // for a quad the vertex_pos and texture coordinates are equal\n"
        "void main() {\n"
        "    vec2 quad_pos = 2 * vertex_pos - vec2(1); // move quad to occupy the whole screen\n"
        "    quad_pos *= vec2(x_ratio, y_ratio); // resize quad to correct aspect ratio\n"
        "    gl_Position = vec4(quad_pos, 0, 1);\n"
        "    uv = vec2(vertex_pos.x, 1 - vertex_pos.y); // pass-through to fragment shader\n"
        "}";
    uint32_t vert_shader_src_size = strlen(vert_shader_src);
    const char* frag_shader_src =
        "#version 330 core\n"
        "in vec2 uv;\n"
        "out vec4 FragColor;\n"
        "uniform sampler2D tex;\n"
        "void main() { FragColor = texture(tex, uv); }";
    uint32_t frag_shader_src_size = strlen(frag_shader_src);
    uint32_t vert_id = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vert_id, 1, &vert_shader_src, &vert_shader_src_size);
    glCompileShader(vert_id);
    uint32_t frag_id = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(frag_id, 1, &frag_shader_src, &frag_shader_src_size);
    glCompileShader(frag_id);
    uint32_t shader_id = glCreateProgram();
    glAttachShader(shader_id, vert_id);
    glAttachShader(shader_id, frag_id);
    glLinkProgram(shader_id);

    // create texture opengl info 
    uint32_t tex_id;
    glGenTextures(1, &tex_id);
    glBindTexture(GL_TEXTURE_2D, tex_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // because of textures whose dimensions are not powers of 2

    // create quad vertices
    float quad_verts[] = {
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
    };
    uint32_t quad_indices[] = { 0, 1, 2, 0, 2, 3 };
    uint32_t quad_vao;
    glGenVertexArrays(1, &quad_vao);
    glBindVertexArray(quad_vao);
    uint32_t quad_vbo;
    glGenBuffers(1, &quad_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, quad_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad_verts), quad_verts, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*) 0);
    glEnableVertexAttribArray(0);
    uint32_t quad_ebo;
    glGenBuffers(1, &quad_ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quad_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(quad_indices), quad_indices, GL_STATIC_DRAW);

    return (QuadWindow) {
        .window = window,
        .quad_handle = quad_vao,
        .shader_handle = shader_id,
        .texture_handle = tex_id,
        .ratio = (float) win_w / (float) win_h,
    };
}

void quad_window_draw(QuadWindow quad_window) {
    int32_t window_w, window_h;
    glfwGetFramebufferSize(quad_window.window, &window_w, &window_h);
    glViewport(0, 0, window_w, window_h);

    float window_size_ratio = (float) window_w / (float) window_h;
    float final_ratio =  window_size_ratio / quad_window.ratio;
    float x_ratio = (quad_window.ratio > window_size_ratio) ? 1 : (1 / final_ratio);
    float y_ratio = (quad_window.ratio > window_size_ratio) ? final_ratio : 1;

    glClear(GL_COLOR_BUFFER_BIT);

    // prepare for rendering
    glUseProgram(quad_window.shader_handle);
    glBindVertexArray(quad_window.quad_handle);
    glUniform1f(glGetUniformLocation(quad_window.shader_handle, "x_ratio"), x_ratio);
    glUniform1f(glGetUniformLocation(quad_window.shader_handle, "y_ratio"), y_ratio);
    glUniform1i(glGetUniformLocation(quad_window.shader_handle, "tex"), 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, quad_window.texture_handle);

    // render quad with our texture
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);

    glfwSwapBuffers(quad_window.window);
    glfwPollEvents();
}

void quad_window_update(QuadWindow* quad_window, uint32_t img_w, uint32_t img_h, uint8_t* data) {
    glBindTexture(GL_TEXTURE_2D, quad_window->texture_handle);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_w, img_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    quad_window->ratio = (float) img_w / (float) img_h;
}

#endif // --------- end of implementation ---------
