srcs=main.c common.c
exec=qoi_viewer
libs=-lglfw -ldl
flags=-O3

all:
	gcc $(srcs) -o $(exec) $(libs) $(flags)
