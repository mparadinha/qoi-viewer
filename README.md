# QOI viewer

Simple image viewer for the [QOI](https://github.com/phoboslab/qoi) format.

## Building 

You need to have GLFW installed.
Run `make`.

## Usage

Use your right and left arrow keys to switch between the images in the list.

Run this with many arguments:
```
./qoi_viewer first_img.qoi second_img.qoi third_img.qoi
```
or with no arguments (it will scan the current directory for all QOI files):
```
./qoi_viewer
```
