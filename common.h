#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define debug_print(...) { \
    printf("[%s:%s:%d] ", __FILE__, __func__, __LINE__); \
    printf(__VA_ARGS__); \
}

#define debug_assert(cond, ...) { \
    if (!(cond)) { \
        fprintf(stderr, "(assertion '%s' failed) [%s:%s:%d] ", #cond, __FILE__, __func__, __LINE__); \
        fprintf(stderr, __VA_ARGS__); \
        abort(); \
    } \
}

#define debug_crash(...) { \
    fprintf(stderr, "(crash) [%s:%s:%d] ", __FILE__, __func__, __LINE__); \
    fprintf(stderr, __VA_ARGS__); \
    abort(); \
}

typedef struct {
    uint8_t* data;
    size_t size;
    size_t pos;
} Buffer;

// the `_at` functions don't change the Buffer struct itself
// but do write to the underlying data buffer, so we take a
// pointer as argument to reinforce that idea

Buffer new_buffer(size_t size);
void free_buffer(Buffer* buf);
Buffer read_file(char* filepath);
void buffer_write(Buffer* buf, void* src, size_t n_bytes);
void buffer_write_at(Buffer* buf, size_t pos, void* src, size_t n_bytes);
// create a gap of `gap_len` bytes in the buffer starting at `gap_start` by moving
// whatever is after the gap forward. (bytes in gap get set to zero)
void buffer_insert_gap(Buffer* buf, size_t gap_start, size_t gap_len);

void write_str(Buffer* buf, char* str);
void write_u8(Buffer* buf, uint8_t val);
void write_u16(Buffer* buf, uint16_t val);
void write_u32(Buffer* buf, uint32_t val);
void write_u64(Buffer* buf, uint64_t val);
void write_i32_at(Buffer* buf, size_t pos, int32_t val);
void write_u64_at(Buffer* buf, size_t pos, uint64_t val);

char* bool_str(bool b);
