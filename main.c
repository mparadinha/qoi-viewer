#define QUAD_WINDOW_IMPLEMENTATION
#include "quad_window.h"

#include "common.h"

#include <dirent.h>

uint32_t read_be_u32(Buffer* buf) {
    uint32_t value =
        (uint32_t) buf->data[buf->pos + 0] << 24 |
        (uint32_t) buf->data[buf->pos + 1] << 16 |
        (uint32_t) buf->data[buf->pos + 2] <<  8 |
        (uint32_t) buf->data[buf->pos + 3];
    buf->pos += 4;
    return value;
}

typedef struct __attribute__((__packed__)) {
    uint8_t r, g, b, a;
} pixel_t;

#define PIXEL_HASH(pixel) ((pixel.r * 3 + pixel.g * 5 + pixel.b * 7 +  pixel.a * 11) % 64)

uint8_t* decode_qoi_file(char* filename, uint32_t* img_w, uint32_t* img_h) {
    Buffer qoi = read_file(filename);

    // read QOI header
    debug_assert(
        qoi.data[0] == 'q' && qoi.data[1] == 'o' && qoi.data[2] == 'i' && qoi.data[3] == 'f',
        "wrong QOI magic bytes\n");
    qoi.pos += 4;
    uint32_t width = read_be_u32(&qoi);
    uint32_t height = read_be_u32(&qoi);
    uint8_t channels = qoi.data[qoi.pos++];
    uint8_t colorspace = qoi.data[qoi.pos++];

    *img_w = width;
    *img_h = height;
    pixel_t* pixels = malloc(width * height * sizeof(pixel_t));

    pixel_t prev_pixel = { .r = 0, .g = 0, .b = 0, .a = 255 };
    pixel_t table[64] = {0};

    size_t pixels_done = 0;
    while (pixels_done < width * height) {
        uint8_t chunk_byte = qoi.data[qoi.pos++];

        if (chunk_byte == 0xfe) { // QOI_OP_RGB
            prev_pixel.r = qoi.data[qoi.pos++];
            prev_pixel.g = qoi.data[qoi.pos++];
            prev_pixel.b = qoi.data[qoi.pos++];
            pixels[pixels_done++] = prev_pixel;
            table[PIXEL_HASH(prev_pixel)] = prev_pixel;
        }
        else if (chunk_byte == 0xff) { // QOI_OP_RGBA
            prev_pixel.r = qoi.data[qoi.pos++];
            prev_pixel.g = qoi.data[qoi.pos++];
            prev_pixel.b = qoi.data[qoi.pos++];
            prev_pixel.a = qoi.data[qoi.pos++];
            pixels[pixels_done++] = prev_pixel;
            table[PIXEL_HASH(prev_pixel)] = prev_pixel;
        }
        else if ((chunk_byte & 0xc0) == 0x00) { // QOI_OP_INDEX
            uint8_t idx = chunk_byte & 0x3f;
            pixels[pixels_done++] = table[idx];
            prev_pixel = table[idx];
        }
        else if ((chunk_byte & 0xc0) == 0x40) { // QOI_OP_DIFF
            prev_pixel.r += ((int8_t) ((chunk_byte & 0x30) >> 4)) - 2;
            prev_pixel.g += ((int8_t) ((chunk_byte & 0x0c) >> 2)) - 2;
            prev_pixel.b += ((int8_t) ((chunk_byte & 0x03)     )) - 2;
            pixels[pixels_done++] = prev_pixel;
            table[PIXEL_HASH(prev_pixel)] = prev_pixel;
        }
        else if ((chunk_byte & 0xc0) == 0x80) { // QOI_OP_LUMA
            int8_t green_diff = ((int8_t) ((chunk_byte & 0x3f)     )) - 32;
            chunk_byte = qoi.data[qoi.pos++];
            int8_t red_green_diff  = ((int8_t) ((chunk_byte & 0xf0) >> 4)) - 8;
            int8_t blue_green_diff = ((int8_t) ((chunk_byte & 0x0f)     )) - 8;
            prev_pixel.g += green_diff;
            prev_pixel.r += (red_green_diff + green_diff);
            prev_pixel.b += (blue_green_diff + green_diff);
            pixels[pixels_done++] = prev_pixel;
            table[PIXEL_HASH(prev_pixel)] = prev_pixel;
        }
        else if ((chunk_byte & 0xc0) == 0xc0) { // QOI_OP_RUN
            uint8_t run_len = (chunk_byte & 0x3f) + 1;
            for (uint32_t i = 0; i < run_len; i++) pixels[pixels_done++] = prev_pixel;
        }
        else {
            debug_crash("chunk_byte=0x%x\n", chunk_byte);
        }
    }

    for (uint32_t i = 0; i < 7; i++) debug_assert(qoi.data[qoi.pos++] == 0x00, "\n");
    debug_assert(qoi.data[qoi.pos++] == 0x01, "\n");

    return (uint8_t*) pixels;
}

int main(int argc, char** argv) {
    // if no arguments are passed in, show all the valid files in the working directory
    char* filenames[100];
    size_t n_filenames = 0;
    if (argc > 1) {
        for (size_t i = 1; i < argc; i++) filenames[n_filenames++] = argv[i];
    }
    else {
        DIR* dir = opendir(".");
        debug_assert(dir, "\n");
        struct dirent* dir_entry = NULL;
        while (true) {
            dir_entry = readdir(dir);
            if (!dir_entry) break;

            char* filename = dir_entry->d_name;
            size_t filename_len = strlen(dir_entry->d_name);
            if (strcmp(&filename[filename_len - 4], ".qoi") != 0) continue;

            char* str_ptr = malloc(filename_len + 1);
            strcpy(str_ptr, filename);
            filenames[n_filenames++] = str_ptr;
        }
    }

    if (n_filenames == 0) {
        printf("no QOI files to show\n");
        return -1;
    }

    size_t cur_filename_idx = 0;
    bool img_needs_update = true;

    QuadWindow quad_window = quad_window_create(1200, 700, "QOI viewer");

    bool last_left_pressed = false;
    bool last_right_pressed = false;
    while (!glfwWindowShouldClose(quad_window.window)) {
        // press left arrow to go back 1 in the image list, right to go forward 1
        bool left_pressed = (glfwGetKey(quad_window.window, GLFW_KEY_LEFT) == GLFW_PRESS);
        bool right_pressed = (glfwGetKey(quad_window.window, GLFW_KEY_RIGHT) == GLFW_PRESS);
        if (last_left_pressed && !left_pressed && n_filenames > 1) {
            cur_filename_idx = (cur_filename_idx == 0) ? (n_filenames - 1) : (cur_filename_idx - 1);
            img_needs_update = true;
        }
        if (last_right_pressed && !right_pressed && n_filenames > 1) {
            cur_filename_idx = (cur_filename_idx + 1) % n_filenames;
            img_needs_update = true;
        }
        last_left_pressed = left_pressed;
        last_right_pressed = right_pressed;

        if (img_needs_update) {
            char* filename = filenames[cur_filename_idx];
            uint32_t img_w, img_h;
            uint8_t* img_data = decode_qoi_file(filename, &img_w, &img_h);
            debug_assert(img_data, "\n");

            quad_window_update(&quad_window, img_w, img_h, img_data);
            img_needs_update = false;
        }

        quad_window_draw(quad_window);
    }
}
