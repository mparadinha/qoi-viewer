#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <execinfo.h>
#include <unistd.h>

Buffer new_buffer(size_t size) {
    Buffer buffer = {0};
    buffer.data = calloc(1, size);
    if(!buffer.data) { debug_print("malloc error\n") }
    else { buffer.size = size; }
    return buffer;
}

void free_buffer(Buffer* buf) {
    free(buf->data);
    buf->data = NULL;
    buf->size = 0;
    buf->pos = 0;
}

Buffer read_file(char* filepath) {
    Buffer buffer = {0};
    FILE* f = fopen(filepath, "r");
    if(!f) {
        debug_print("error opening file '%s'\n", filepath)
        return buffer;
    }

    fseek(f, 0, SEEK_END);
    buffer.size = ftell(f);
    buffer.data = malloc(buffer.size);
    if(!buffer.data) {
        debug_print("out of memory (file '%s')\n", filepath)
        return buffer;
    }

    fseek(f, 0, SEEK_SET);
    debug_assert(fread(buffer.data, 1, buffer.size, f) == buffer.size,
        "read less bytes from file '%s' than supposted to\n", filepath);

    fclose(f);
    return buffer;
}

void buffer_write(Buffer* buf, void* src, size_t n_bytes) {
    memcpy(buf->data + buf->pos, src, n_bytes);
    buf->pos += n_bytes;
}

void buffer_write_at(Buffer* buf, size_t pos, void* src, size_t n_bytes) {
    memcpy(buf->data + pos, src, n_bytes);
}


void buffer_insert_gap(Buffer* buf, size_t gap_start, size_t gap_len) {
    memmove(
        buf->data + gap_start + gap_len,
        buf->data + gap_start,
        buf->pos - gap_start
    );
    memset(buf->data + gap_start, 0, gap_len);
    buf->pos += gap_len;
}

void write_str(Buffer* buf, char* str) {
    size_t bytes_left = buf->size - buf->pos;
    size_t str_len = strlen(str);
    debug_assert(str_len + 1 <= bytes_left,
        "str_len + '\\0'=%lu vs bytes_left=%lu\n", str_len + 1, bytes_left);
    strcpy(buf->data + buf->pos, str);
    buf->pos += str_len + 1;
}

void write_u8(Buffer* buf, uint8_t val) {
    buf->data[buf->pos++] = val;
}

void write_u16(Buffer* buf, uint16_t val) {
    *((uint16_t*) (buf->data + buf->pos)) = val; buf->pos += 2;
}

void write_u32(Buffer* buf, uint32_t val) {
    *((uint32_t*) (buf->data + buf->pos)) = val; buf->pos += 4;
}

void write_u64(Buffer* buf, uint64_t val) {
    *((uint64_t*) (buf->data + buf->pos)) = val; buf->pos += 8;
}

void write_i32_at(Buffer* buf, size_t pos, int32_t val) {
    *((int32_t*) (buf->data + pos)) = val;
}

void write_u64_at(Buffer* buf, size_t pos, uint64_t val) {
    *((uint64_t*) (buf->data + pos)) = val;
}

char* bool_str(bool b) {
    return b ? "true":"false";
}
